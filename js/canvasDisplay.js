var canvas;
var ctx;

var imgBackground;
var imgTipo;
var imgQuadro;
var imgGarfo;
var imgGuidao;
var imgAro;
var imgPedivela;

var imgArray;

var precoBase;
var precoAdicionalTipo;
var precoAdicionalTamanho;
var precoAdicionalGuidao;

var tempoBase;
var tempoAdicionalTipo;
var tempoAdicionalTamanho;
var tempoAdicionalQuadro;
var tempoAdicionalGarfo;
var tempoAdicionalGuidao;
var tempoAdicionalAro;
var tempoAdicionalPedivela;

var mensagens;
var mensagemTipo;
var mensagemTamanho;
var mensagemQuadro;
var mensagemGarfo;
var mensagemGuidao;
var mensagemAro;
var mensagemPedivela;


NProgress.start();
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        initApp();
        NProgress.done();
    }
}

$(function () {

});

$(window).resize(function () {
    drawAllParts();
});

function initApp() {
    console.log("DOCUMENT READY")
    canvas = document.getElementById("myCanvas");
    canvas = new fabric.Canvas('myCanvas');

    imgBackground = new fabric.Image(document.getElementById("backgroundCidade"), { selectable: false })
    imgTipo = new fabric.Image(document.getElementById("tipoRodaLivre"), { selectable: false })
    imgQuadro = new fabric.Image(document.getElementById("quadroBranco"), { selectable: false })
    imgGarfo = new fabric.Image(document.getElementById("garfoBranco"), { selectable: false })
    imgGuidao = new fabric.Image(document.getElementById("guidaoRiser"), { selectable: false })
    imgAro = new fabric.Image(document.getElementById("aroPreto"), { selectable: false })
    imgPedivela = new fabric.Image(document.getElementById("pedivelaPreto"), { selectable: false })

    imgArray = [imgBackground, imgTipo, imgQuadro, imgGarfo, imgGuidao, imgAro, imgPedivela];

    precoBase = 170000;
    precoAdicionalTipo = 0;
    precoAdicionalTamanho = 0;
    precoAdicionalGuidao = 0;

    tempoBase = 3;
    tempoAdicionalTipo = 0;
    tempoAdicionalTamanho = 0;
    tempoAdicionalQuadro = 0;
    tempoAdicionalGarfo = 0;
    tempoAdicionalGuidao = 0;
    tempoAdicionalAro = 0;
    tempoAdicionalPedivela = 0;

    drawAllParts();
    updateTexts();
}

function selectedBackground(background) {
    drawByType("background", background);
    updateTexts();
}

function selectedTipo(tipo) {
    precoAdicionalTipo = 0;
    switch (tipo.value) {
        case "Fixa":
            precoAdicionalTipo = 20000;
            break;
    }

    drawByType("tipo", tipo);
    updateTexts();
}

function selectedTamanho(tamanho) {
    precoAdicionalTamanho = 0;
    tempoAdicionalTamanho = 0;
    mensagemTamanho = "";
    switch (tamanho.value) {
        case "Mixte45":
        case "Mixte48":
            precoAdicionalTamanho = 10000;
            mensagemTamanho = "Disponibilidade não é certa.";
            break;
        case "SobMedida":
            precoAdicionalTamanho = 20000;
            tempoAdicionalTamanho = 35;
            mensagemTamanho = "Atenção para o tempo de entrega.";
            break;
    }

    updateTexts();
}

function selectedQuadro(quadroCor) {
    tempoAdicionalQuadro = 0;
    switch (quadroCor.value) {
        case "Branco":
            tempoAdicionalQuadro = 0;
            break;
        default:
            tempoAdicionalQuadro = 7;
            break;
    }

    drawByType("quadro", quadroCor);
    updateTexts();
}

function selectedGarfo(garfoCor) {
    tempoAdicionalGarfo = 0;
    switch (garfoCor.value) {
        case "Branco":
            tempoAdicionalGarfo = 0;
            break;
        default:
            tempoAdicionalGarfo = 7;
            break;
    }

    drawByType("garfo", garfoCor);
    updateTexts();
}

function selectedGuidao(guidao) {
    precoAdicionalGuidao = 0;
    mensagemGuidao = "";
    switch (guidao.value) {
        case "Bullhorn":
            precoAdicionalGuidao = 10000;
            mensagemTamanho = "Disponibilidade não é certa.";
            break;
        case "Drop":
            mensagemGuidao = "Não inclui os freios";
            break;
    }

    drawByType("guidao", guidao);
    updateTexts();
}

function selectedAro(aroCor) {
    tempoAdicionalAro = 0;
    switch (aroCor.value) {
        case "Preto":
            tempoAdicionalAro = 0;
            break;
        default:
            tempoAdicionalAro = 7;
            break;
    }

    drawByType("aro", aroCor);
    updateTexts();
}

function selectedPedivela(pedivelaCor) {
    tempoAdicionalPedivela = 0;
    mensagemPedivela = "";
    switch (pedivelaCor.value) {
        case "Prata":
            tempoAdicionalPedivela = 10;
            break;
    }

    drawByType("pedivela", pedivelaCor);
    updateTexts();
}

function drawByType(type, selected) {
    switch (type) {
        case "background":
            imgBackground = new fabric.Image(document.getElementById("background" + selected.value), { selectable: false })
            break;
        case "tipo":
            imgTipo = new fabric.Image(document.getElementById("tipo" + selected.value), { selectable: false })
            break;
        case "quadro":
            imgQuadro = new fabric.Image(document.getElementById("quadro" + selected.value), { selectable: false })
            break;
        case "garfo":
            imgGarfo = new fabric.Image(document.getElementById("garfo" + selected.value), { selectable: false })
            break;
        case "guidao":
            imgGuidao = new fabric.Image(document.getElementById("guidao" + selected.value), { selectable: false })
            break;
        case "aro":
            imgAro = new fabric.Image(document.getElementById("aro" + selected.value), { selectable: false })
            break;
        case "pedivela":
            imgPedivela = new fabric.Image(document.getElementById("pedivela" + selected.value), { selectable: false })
            break;
    }
    drawAllParts();
}

function drawAllParts() {
    canvas.clear();
    drawImage(imgBackground);
    drawImage(imgTipo);
    drawImage(imgAro);
    drawImage(imgQuadro);
    drawImage(imgGarfo);
    drawImage(imgGuidao);
    drawImage(imgPedivela);
    recalculateCanvasSize();
}

function drawImage(img) {
    canvas.add(img);
    canvas.centerObject(img);
    img.scaleToHeight(canvas.getHeight());
}

function updateTexts() {
    updatePreco();
    updateTempo();
    updateMensagens();
}

function updatePreco() {
    preco = precoBase + precoAdicionalTipo + precoAdicionalTamanho + precoAdicionalGuidao;
    $(".preco-text").text(formatReal(preco));
}

function updateTempo() {
    tempo = tempoBase + tempoAdicionalTipo + tempoAdicionalTamanho + tempoAdicionalQuadro + tempoAdicionalGarfo + tempoAdicionalGuidao + tempoAdicionalAro + tempoAdicionalPedivela;
    $(".tempo-text").text(tempo + " dias");
}

function updateMensagens() {
    mensagens = [mensagemTipo, mensagemTamanho, mensagemQuadro, mensagemGarfo, mensagemGuidao, mensagemAro, mensagemPedivela];
    var mensagensLocal = "Observações: <ul class='list-unstyled'>";
    var existeMensagem = false;
    for (i = 0; i < mensagens.length; i++) {
        if (mensagens[i]) {
            mensagensLocal += '<li class="small">' + mensagens[i] + "</li>";
            existeMensagem = true;
        }
    }
    mensagensLocal += "</ul>";

    if (existeMensagem) {
        $(".mensagens").html(mensagensLocal);
    } else {
        $(".mensagens").html("");
    }
}

function getMoney(str) {
    return parseInt(str.replace(/[\D]+/g, ''));
}

function formatReal(input) {
    var tmp = input + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6) {
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }
    return "R$ " + tmp;
}

function recalculateCanvasSize() {
    var newWidth = $("#canvasDiv").width()
    var newHeigth = parseInt(newWidth / 16 * 9);
    canvas.setWidth(newWidth);
    canvas.setHeight(newHeigth);

    if (imgArray.length) {
        imgArray.forEach(function (imgFabric) {
            imgFabric.scaleToHeight(canvas.getHeight());
            canvas.centerObject(imgFabric);
        }, this);
    }

    canvas.renderAll();
}