$(function () {
    drawFooterMap();
});

function drawFooterMap() {
    var divWidth = $("#addressMapDiv").width()
    var divHeight = $("#addressMapDiv").height()
    var imgSrc = "https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDfYVDN031mnd1dsZYshsKk_hNYamwN0LI&markers=Bitelli+Bikes&center=Bitelli+Bikes&zoom=17&size=" + divWidth + "x" + divHeight + "&sensor=false&scale=2";
    $("#addressMapImg").attr("src", imgSrc);
}