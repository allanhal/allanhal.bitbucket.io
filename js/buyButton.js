_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

$(function () {
    $("#buyButton").click(function () {
        buyButtonClicked();
    });
});

function buyButtonClicked() {
    Backbone.emulateHTTP = true;

    var pedido = new Backbone.Model();
    pedido.urlRoot = 'https://rest-on-demand.herokuapp.com/api/pedido';
    // pedido.urlRoot = 'http://localhost:5000/api/pedido';
    pedido.set({
        'nome': $("#inputNome").val(),
        'telefone': $("#inputTelefone").val(),
        'email': $("#inputEmail").val(),
        'tipo': $("select#tipo").val(),
        'tamanho': $("select#tamanho").val(),
        'quadro': $("select#quadro").val(),
        'garfo': $("select#garfo").val(),
        'guidao': $("select#guidao").val(),
        'aro': $("select#aro").val(),
        'pedivela': $("select#pedivela").val(),
        'preco': $(".preco-text").first().text(),
        'tempo': $(".tempo-text").first().text(),
    });
    pedido.save({}, {
        success: function (model, response) {
            showAlert("alertEnvioDados", "success", "Dados enviados. Entraremos em contato.")
            console.log('success');
        },
        error: function (x, y, z) {
            console.log('error');
            console.log(x);
            console.log(y);
            console.log(z);
        }
    });
}

function showAlert(containerId, alertType, message) {
    $("#" + containerId).append('<div class="alert alert-' + alertType + '" id="alert' + containerId + '">' + message + '</div>');
    $("#alert" + containerId).alert();
    window.setTimeout(function () { $("#alert" + containerId).alert('close'); }, 5000);
}