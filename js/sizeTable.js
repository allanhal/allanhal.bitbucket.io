$(function () {
    $("#userSize").keyup(function () {
        changedUserSize(parseInt($("#userSize").val()));
    });
});

function changedUserSize(size) {
    clearTable();
    if (!Number.isInteger(size)) {
        return;
    }

    if (size >= 150 && size <= 155) {
        $("#sizeTable0").addClass("bg-primary");
    }
    if (size >= 155 && size <= 160) {
        $("#sizeTable1").addClass("bg-primary");
    }
    if (size >= 156 && size <= 160) {
        $("#sizeTable2").addClass("bg-primary");
    }
    if (size >= 160 && size <= 164) {
        $("#sizeTable3").addClass("bg-primary");
    }
    if (size >= 164 && size <= 168) {
        $("#sizeTable4").addClass("bg-primary");
    }
    if (size >= 168 && size <= 172) {
        $("#sizeTable5").addClass("bg-primary");
    }
    if (size >= 172 && size <= 176) {
        $("#sizeTable6").addClass("bg-primary");
    }
    if (size >= 176 && size <= 180) {
        $("#sizeTable7").addClass("bg-primary");
    }
    if (size >= 180 && size <= 184) {
        $("#sizeTable8").addClass("bg-primary");
    }
    if (size >= 184 && size <= 190) {
        $("#sizeTable9").addClass("bg-primary");
    }
    if (size >= 145 && size <= 200) {
        $("#sizeTable10").addClass("bg-primary");
    } else if ((size > 120 && size < 145) || (size > 200 && size < 220)) {
        $("#sizeTable10").addClass("bg-danger");
        
    }
}

function clearTable() {
    for (var i = 0; i < 11; i++) {
        $("#sizeTable" + i).removeClass("bg-primary");
    }
    $("#sizeTable10").removeClass("bg-danger");
}