var cvs1;
var imgElement;
var oImg1;

$(function () {
    cvs1 = new fabric.Canvas('myCanvas');
    imgElement = document.getElementById('tradicionalImg');
});

$(window).load(function () {
    createImage();
    recalculateCanvasSize();
});

$(window).resize(function () {
    recalculateCanvasSize();
});

function createImage(params) {
    this.oImg1 = new fabric.Image(this.imgElement, { selectable: false })
    cvs1.add(this.oImg1);
}

function recalculateCanvasSize() {
    var newWidth = $("#canvasDiv").width()
    var newHeigth = parseInt(newWidth / 16 * 9);
    cvs1.setWidth(newWidth);
    cvs1.setHeight(newHeigth);
    if (this.oImg1) {
        this.oImg1.scaleToHeight(cvs1.getHeight());
        cvs1.centerObject(this.oImg1);
    }
    cvs1.renderAll();
}