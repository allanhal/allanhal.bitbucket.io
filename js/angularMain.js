// script.js

var bitelliApp = angular.module('bitelliApp', []);

// create the controller and inject Angular's $scope
bitelliApp.controller('mainController', function ($scope) {
    // create a message to display in our view
    $scope.pedido = {};
    $scope.pedido.background = 'Cidade';
    $scope.pedido.tipo = 'RodaLivre';
    $scope.pedido.tamanho = 'Catalina46';
    $scope.pedido.quadro = 'Branco';
    $scope.pedido.garfo = 'Branco';
    $scope.pedido.guidao = 'Riser';
    $scope.pedido.aro = 'Preto';
    $scope.pedido.pedivela = 'Preto';

});